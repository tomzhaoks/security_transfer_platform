# 安全传输平台( Security Transfer Platform), 简称(STP)

# 使用的开源库：
libevent-2.0.22-stable

protobuf-2.5.0

glog-0.3.4

mysql-5.6.25


# 环境安装
> sudo yum install libevent-devel.x86_64 libevent.x86_64 libevent-doc.noarch

> sudo yum install protobuf-devel.x86_64 protobuf.x86_64

> sudo yum install glog-devel.x86_64 glog.x86_64

> sudo yum install gflags.x86_64 gflags-devel.x86_64

> sudo yum install gperftools-devel.x86_64 gperftools.x86_64

> sudo yum info mariadb-server.x86_64 mariadb.x86_64 mariadb-devel.x86_64


# 获取security_transfer_platform
> git clone https://gitee.com/lsgx/security_transfer_platform.git

> cd security_transfer_platform

> git submodule update --init --recursive


# 构建 libmysqlcppconn
> mkdir build

> cd build

> cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_COLOR_MAKEFILE=ON ../src/libmysqlcppconn -DCMAKE_TOOLCHAIN_FILE=toolchain-Debug.cmake

> gmake help

> gmake all

> tree ../src/dist/

```
[root@lsgxeva build]# tree ../src/dist/
../src/dist/
├── bin
│   ├── mysql_unittest
│   ├── mysql_unittest2
│   └── mysql_unittest3
└── lib
    └── libmysqlcppconn.so

2 directories, 4 files
```

# 安装 libmysqlcppconn
> gmake install

```
[root@lsgxeva build]# gmake install
[ 45%] Built target mysqlcppconn
[ 63%] Built target mysql_unittest3
[ 81%] Built target mysql_unittest2
[100%] Built target mysql_unittest
Install the project...
-- Install configuration: "Debug"
-- Installing: /usr/local/include/connection.h
-- Installing: /usr/local/include/connection_pool.h
-- Installing: /usr/local/include/resultset.h
-- Installing: /usr/local/include/statement.h
-- Installing: /usr/local/lib/libmysqlcppconn.so
-- Set runtime path of "/usr/local/lib/libmysqlcppconn.so" to ""
```

> tree /usr/local/include/

```
[root@lsgxeva build]# tree /usr/local/include/
/usr/local/include/
├── connection.h
├── connection_pool.h
├── resultset.h
└── statement.h

0 directories, 4 files
```

> tree /usr/local/lib/

```
[root@lsgxeva build]# tree /usr/local/lib/
/usr/local/lib/
└── libmysqlcppconn.so

0 directories, 1 file
```

# 清理 libmysqlcppconn
> gmake clean

> cd ..

> rm -rf build

> rm -rf src/dist


# 构建 stpserver
> mkdir build

> cd build

> cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_COLOR_MAKEFILE=ON ../src -DCMAKE_TOOLCHAIN_FILE=toolchain-Debug.cmake

> gmake help

> gmake all

> tree ../dist/

```
[root@lsgxeva build]# tree ../dist/
../dist/
├── bin
│   ├── atomic_test
│   ├── mutex_test
│   ├── stpclient
│   ├── stpserver
│   ├── tcp_client_test
│   ├── tcp_server_test
│   ├── thread_test
│   └── token_test
└── lib
    ├── libcodec.so
    ├── libstpcomm.so
    ├── libstpcrypto.so
    └── libstputil.so

2 directories, 12 files
```

# 安装 stpserver
> gmake install

```
[root@lsgxeva build]# gmake install
[  7%] Built target codec
[ 39%] Built target stputil
[ 43%] Built target atomic_test
[ 47%] Built target mutex_test
[ 50%] Built target thread_test
[ 54%] Built target tcp_server_test
[ 58%] Built target tcp_client_test
[ 69%] Built target stpcomm
[ 73%] Built target token_test
[ 81%] Built target stpcrypto
[ 90%] Built target stpserver
[100%] Built target stpclient
Install the project...
-- Install configuration: "Debug"
-- Installing: /usr/local/lib/libcodec.so
-- Set runtime path of "/usr/local/lib/libcodec.so" to ""
-- Installing: /usr/local/include/stpcomm/atomic.h
-- Installing: /usr/local/include/stpcomm/connection.h
-- Installing: /usr/local/include/stpcomm/event_notifier.h
-- Installing: /usr/local/include/stpcomm/json_parser.h
-- Installing: /usr/local/include/stpcomm/mutex.h
-- Installing: /usr/local/include/stpcomm/shm.h
-- Installing: /usr/local/include/stpcomm/tcp_event_client.h
-- Installing: /usr/local/include/stpcomm/tcp_event_server.h
-- Installing: /usr/local/include/stpcomm/thread.h
-- Installing: /usr/local/lib/libstputil.so
-- Set runtime path of "/usr/local/lib/libstputil.so" to ""
-- Installing: /usr/local/lib/libstpcomm.so
-- Set runtime path of "/usr/local/lib/libstpcomm.so" to ""
-- Installing: /usr/local/lib/libstpcrypto.so
```

> tree /usr/local/include/

```
[root@lsgxeva build]# tree /usr/local/include/
/usr/local/include/
├── connection.h
├── connection_pool.h
├── resultset.h
├── statement.h
└── stpcomm
    ├── atomic.h
    ├── connection.h
    ├── event_notifier.h
    ├── json_parser.h
    ├── mutex.h
    ├── shm.h
    ├── tcp_event_client.h
    ├── tcp_event_server.h
    └── thread.h

1 directory, 13 files
```

> tree /usr/local/lib/

```
[root@lsgxeva build]# tree /usr/local/lib/
/usr/local/lib/
├── libcodec.so
├── libmysqlcppconn.so
├── libstpcomm.so
├── libstpcrypto.so
└── libstputil.so

0 directories, 5 files
```

# 清理 stpserver
> gmake clean

> cd ..

> rm -rf build

> rm -rf dist


# 卸载 libmysqlcppconn
> rm -rf /usr/local/include/connection.h /usr/local/include/connection_pool.h /usr/local/include/resultset.h /usr/local/include/statement.h

> rm -rf /usr/local/lib/libmysqlcppconn.so

# 卸载 stpserver
> rm -rf /usr/local/include/stpcomm

> rm -rf /usr/local/lib/libcodec.so /usr/local/lib/libstputil.so /usr/local/lib/libstpcomm.so /usr/local/lib/libstpcrypto.so

