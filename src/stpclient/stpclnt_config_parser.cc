#include "stpclnt_config_parser.h"

StpClientConfigParser &StpClientConfigParser::GetInstance()
{
    static StpClientConfigParser s_Instance;
    return s_Instance;
}

StpClientConfigParser::StpClientConfigParser()
{

}

StpClientConfigParser::~StpClientConfigParser()
{

}

bool StpClientConfigParser::ParseProfile(const char *file)
{
    JsonParser parser;

	if (!parser.Parse(file))
		return false;

    svr_ip_ = parser.GetString("svr_ip", "127.0.0.1");
    svr_port_ = static_cast<int16_t>(parser.GetInteger("svr_port", 9999));

    path_ = parser.GetString("path", "");
    flag_ = parser.GetInteger("max_idx", 100);
    stp_guid_ = parser.GetUint64("stp_guid", 0);
    max_idx_ = parser.GetInteger("max_idx", 1);

	return true;
}


