#
# Filename: CMakeLists.txt
# Usage:
#     $ mkdir build
#     $ cd build
#     $ cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_COLOR_MAKEFILE=ON ../src -DCMAKE_TOOLCHAIN_FILE=toolchain-Debug.cmake
#     $ gmake V=1 all
#

cmake_minimum_required (VERSION 2.8)

set(PROJECT_NAME "SecureTransferPlantform")
project(${PROJECT_NAME} C CXX)

set(PROJECT_VERSION_MAJOR 1)
set(PROJECT_VERSION_MINOR 0)
set(PROJECT_VERSION_PATCH 1)
set(PROJECT_VERSION_TWEAK 1)

enable_testing()

message(STATUS "operation system is ${CMAKE_SYSTEM}")

if (CMAKE_SYSTEM_NAME MATCHES "Linux")
    message(STATUS "current platform: Linux")
elseif (CMAKE_SYSTEM_NAME MATCHES "Windows")
    message(STATUS "current platform: Windows")
elseif (CMAKE_SYSTEM_NAME MATCHES "FreeBSD")
    message(STATUS "current platform: FreeBSD")
else ()
    message(STATUS "other platform: ${CMAKE_SYSTEM_NAME}")
endif (CMAKE_SYSTEM_NAME MATCHES "Linux")

if (WIN32)
    message(STATUS "Now is windows")
    message(STATUS "Note: Define UNICODE environment variable")
endif (WIN32)

if (APPLE)
    message(STATUS "Now is Apple systens.")
endif (APPLE)

if (UNIX)
    message(STATUS "Now is UNIX-like OS's. Including APPLE OS x and CygWin")
endif (UNIX)


if (CMAKE_BUILD_TYPE MATCHES "Debug" OR CMAKE_BUILD_TYPE MATCHES "None")
    message(STATUS "CMAKE_BUILD_TYPE is Debug")
elseif (CMAKE_BUILD_TYPE MATCHES "Release")
    message(STATUS "CMAKE_BUILD_TYPE is Release")
elseif (CMAKE_BUILD_TYPE MATCHES "RelWitchDebInfo")
    message(STATUS "CMAKE_BUILD_TYPE is RelWitchDebInfo")
elseif (CMAKE_BUILD_TYPE MATCHES "MinSizeRel")
    message(STATUS "CMAKE_BUILD_TYPE is MinSizeRel")
else ()
    message(FATAL_ERROR "Error: unknown CMAKE_BUILD_TYPE = ${CMAKE_BUILD_TYPE},"
            " CMAKE_BUILD_TYPE: Debug or None, Release, RelWitchDebInfo, RelWitchDebInfo")
endif ()


if (NOT CMAKE_COMPILER_IS_GNUCC AND NOT CMAKE_COMPILER_IS_GNUCXX AND NOT MSVC)
    message(FATAL_ERROR "Error: ${CMAKE_C_COMPILER} need requires gcc/g++ or msvc.")
else (NOT CMAKE_COMPILER_IS_GNUCC AND NOT CMAKE_COMPILER_IS_GNUCXX AND NOT MSVC)
    message(STATUS "Note: c compiler - ${CMAKE_C_COMPILER}")
    message(STATUS "Note: cxx compiler - ${CMAKE_CXX_COMPILER}")
endif (NOT CMAKE_COMPILER_IS_GNUCC AND NOT CMAKE_COMPILER_IS_GNUCXX AND NOT MSVC)

if (CMAKE_COMPILER_IS_GNUCC)
    EXECUTE_PROCESS(COMMAND ${CMAKE_C_COMPILER} -dumpversion OUTPUT_VARIABLE GNUCC_VERSION)
    if (GNUCC_VERSION VERSION_LESS 4.7)
        message(FATAL_ERROR "Error: GNUCC need requires gcc 4.7 or greater.")
    else (GNUCC_VERSION VERSION_LESS 4.7)
        message(STATUS "Note: GNUCC current version ${GNUCC_VERSION}")
    endif (GNUCC_VERSION VERSION_LESS 4.7)
endif (CMAKE_COMPILER_IS_GNUCC)

if (CMAKE_COMPILER_IS_GNUCXX)
    EXECUTE_PROCESS(COMMAND ${CMAKE_CXX_COMPILER} -dumpversion OUTPUT_VARIABLE GNUCXX_VERSION)
    if (GNUCXX_VERSION VERSION_LESS 4.7)
        message(FATAL_ERROR "Error: GNUCXX need requires g++ 4.7 or greater.")
    else (GNUCXX_VERSION VERSION_LESS 4.7)
        message(STATUS "Note: GNUCXX current version ${GNUCXX_VERSION}")
    endif (GNUCXX_VERSION VERSION_LESS 4.7)
endif (CMAKE_COMPILER_IS_GNUCXX)

if (MSVC)
    if (MSVC_VERSION LESS 1700)
        message(FATAL_ERROR "Error: MSVC need requires vs2012 or greater.")
    else (MSVC_VERSION LESS 1700)
        message(STATUS "Note: MSVC current version ${MSVC_VERSION}")
    endif (MSVC_VERSION LESS 1700)
endif (MSVC)


set(PROJECT_DIST_DIR ${PROJECT_SOURCE_DIR}/../dist)
file(MAKE_DIRECTORY ${PROJECT_DIST_DIR} ${PROJECT_DIST_DIR}/bin ${PROJECT_DIST_DIR}/lib)
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_DIST_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${PROJECT_DIST_DIR}/lib)

include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/include ${CMAKE_CURRENT_SOURCE_DIR}../include)
link_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/lib ${CMAKE_CURRENT_SOURCE_DIR}../lib)

if (CMAKE_BUILD_TYPE MATCHES "Debug" OR CMAKE_BUILD_TYPE MATCHES "None")
    message(STATUS "Note: Define debug environment variable")
    add_definitions(-D _DEBUG_CDB)
    add_definitions(-D DEBUG)
    add_definitions(-D VALGRIND)
    set(MY_EXECUTABLE_NAME "${PROJECT_NAME}_debug.exe")
    set(MY_LIBRARY_NAME "${PROJECT_NAME}_debug.lib")
else (CMAKE_BUILD_TYPE MATCHES "Debug" OR CMAKE_BUILD_TYPE MATCHES "None")
    message(STATUS "Note: Define non debug environment variable")
    add_definitions(-D NDEBUG)
    set(MY_EXECUTABLE_NAME "${PROJECT_NAME}.exe")
    set(MY_LIBRARY_NAME "${PROJECT_NAME}.lib")
endif (CMAKE_BUILD_TYPE MATCHES "Debug" OR CMAKE_BUILD_TYPE MATCHES "None")

if (WIN32)
    message(STATUS "Note: Define UNICODE environment variable")
    add_definitions(-D _UNICODE)
    add_definitions(-D UNICODE)
endif (WIN32)

if (CMAKE_COMPILER_IS_GNUCC)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fPIC -std=c99")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -Wfloat-equal -funsigned-char")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wshadow -Wpointer-arith -Wcast-qual -Wcast-align")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-unused-variable -Wno-unused-parameter -Wno-reorder -Wno-unused-function -Wno-unused-local-typedefs")
    set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS} -s -O3 -Os -fno-omit-frame-pointer -fno-strict-aliasing -finline-limit=1000")
    set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -O0 -g -ggdb -pipe -ffast-math")
    set(CMAKE_C_COMPILE_OBJECT "${CMAKE_C_COMPILE_OBJECT} ")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,--export-dynamic")
endif (CMAKE_COMPILER_IS_GNUCC)

if (CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -std=c++11")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wfloat-equal -funsigned-char")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wshadow -Wpointer-arith -Wcast-qual -Wcast-align")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-unused-variable -Wno-unused-parameter -Wno-reorder -Wno-unused-function -Wno-unused-local-typedefs")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wold-style-cast -Woverloaded-virtual -Wwrite-strings")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -s -O3 -Os -fno-omit-frame-pointer -fno-strict-aliasing -finline-limit=1000")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0 -g -ggdb -pipe -ffast-math")
    set(CMAKE_CXX_COMPILE_OBJECT "${CMAKE_CXX_COMPILE_OBJECT} ")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,--export-dynamic")
endif (CMAKE_COMPILER_IS_GNUCXX)

if (MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /EHsc /wd\"4819\" /wd\"4996\"")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /Zi /Ob0 /Od /RTC1 /D_DEBUG")
    set(CMAKE_CXX_FLAGS_MINSIZEREL "${CMAKE_CXX_FLAGS_MINSIZEREL} /O1 /Ob1 /D NDEBUG")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /O2 /Ob2 /D NDEBUG")
    set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} /Zi /O2 /Ob1 /D NDEBUG")
    set(CMAKE_CXX_COMPILE_OBJECT "${CMAKE_CXX_COMPILE_OBJECT} ")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ")
endif (MSVC)

if(CMAKE_BUILD_BITS EQUAL 32)
    message(STATUS "Note: Cmake build bits is 32.")
    message(FATAL_ERROR "Error: Stop building here - can not building 32bit programs")

    if (CMAKE_COMPILER_IS_GNUCC)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -m32 -march=i386 -mtune=i386")
        set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} –m elf_i386")
    endif (CMAKE_COMPILER_IS_GNUCC)

    if (CMAKE_COMPILER_IS_GNUCXX)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m32 -march=i386 -mtune=i386")
        set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} –m elf_i386")
    endif (CMAKE_COMPILER_IS_GNUCXX)

elseif (CMAKE_BUILD_BITS EQUAL 64)
    message(STATUS "Note: Cmake build bits is 64.")

else ()
    message(STATUS "Note: Cmake build bits is unknown.")

endif ()


if (CMAKE_COMPILER_IS_GNUCC)
    # add_definitions(-D __STDC_LIMIT_MACROS)
    # add_definitions(-D __STDC_CONSTANT_MACROS)
    # add_definitions(-D __STDC_FORMAT_MACROS)
endif (CMAKE_COMPILER_IS_GNUCC)

if (CMAKE_COMPILER_IS_GNUCXX)
    # add_definitions(-D MAIN_USE_ENVP)
endif (CMAKE_COMPILER_IS_GNUCXX)

if (CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
    # add_definitions(-D MAIN_USE_ENVP)
    add_definitions(-D CHECK_PTHREAD_RETURN_VALUE)
    add_definitions(-D _FILE_OFFSET_BITS=64)
endif (CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)

if (MSVC)
    # add_definitions(-D _AFX_SECURE_NO_WARNINGS)
    # add_definitions(-D _ATL_SECURE_NO_WARNINGS)
    # add_definitions(-D _CRT_SECURE_NO_WARNINGS)
    # add_definitions(-D _CRT_NONSTDC_NO_WARNINGS)
    # add_definitions(-D _SCL_SECURE_NO_WARNINGS)
    # add_definitions(-D NOMINMAX)
endif (MSVC)


message(STATUS ">>>>>> Print cmake environment variable information ...")

message(STATUS "This is the cmake sizeof void p: ${CMAKE_SIZEOF_VOID_P}")
message(STATUS "This is the cmake module path: ${CMAKE_MODULE_PATH}")
message(STATUS "This is the cmake build bits: ${CMAKE_BUILD_BITS}")
message(STATUS "This is the cmake current list file: ${CMAKE_CURRENT_LIST_FILE}")
message(STATUS "This is the cmake current list line: ${CMAKE_CURRENT_LIST_LINE}")
message(STATUS "This is the cmake archive output directory: ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}")
message(STATUS "This is the cmake library output directory: ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}")
message(STATUS "This is the cmake runtime output directory: ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")
message(STATUS "This is the project name: ${PROJECT_NAME}")
message(STATUS "This is the project binary dir: ${PROJECT_BINARY_DIR}")
message(STATUS "This is the project source dir: ${PROJECT_SOURCE_DIR}")
message(STATUS "This is the executable output path: ${EXECUTABLE_OUTPUT_PATH}")
message(STATUS "This is the build shared libs: ${BUILD_SHARED_LIBS}")
message(STATUS "This is the library output path: ${LIBRARY_OUTPUT_PATH}")

message(STATUS "This is the cmake c compiler: ${CMAKE_C_COMPILER}")
message(STATUS "This is the cmake cxx compiler: ${CMAKE_CXX_COMPILER}")
message(STATUS "This is the cmake c compiler object: ${CMAKE_C_COMPILE_OBJECT}")
message(STATUS "This is the cmake cxx compiler object: ${CMAKE_CXX_COMPILE_OBJECT}")

string(TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE)
message(STATUS "C_FLAGS = " ${CMAKE_C_FLAGS} " " ${CMAKE_C_FLAGS_${BUILD_TYPE}})
message(STATUS "CXX_FLAGS = " ${CMAKE_CXX_FLAGS} " " ${CMAKE_CXX_FLAGS_${BUILD_TYPE}})
message(STATUS "EXE_LINKER_FLAGS = " ${CMAKE_EXE_LINKER_FLAGS} " " ${CMAKE_EXE_LINKER_FLAGS_${BUILD_TYPE}})

message(STATUS "<<<<<< End (Print cmake environment variable information ...)")

add_subdirectory(codec)
add_subdirectory(stputil)
add_subdirectory(stpcomm)
add_subdirectory(stpcrypto)
add_subdirectory(stpserver)
add_subdirectory(stpclient)
